CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Field Migration module provides custom functionality to migrate the orders fields from Drupal 7 to Drupal 9. 

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/fields_migration

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/fields_migration


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.




INSTALLATION
------------

 * Install the field migration module as you would normally install a
   contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module and the
       submodule Context UI.
    2. Navigate to /admin/batch-order-update-form to perform the migration.
   


MAINTAINERS
-----------

Current maintainers:
 * Sushant Dhungel - https://git.drupalcode.org/Sushant-D

