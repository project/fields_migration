<?php
namespace Drupal\fields_migration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * An fields_migration controller.
 */
class GeneralController extends ControllerBase {}