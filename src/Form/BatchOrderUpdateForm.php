<?php

namespace Drupal\fields_migration\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Database\Database;
use Drupal\commerce_order\Entity\Order;

/**
 * Implements a Batch example Form.
 */
class BatchOrderUpdateForm extends FormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'BatchOrderUpdateForm';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $orders_count = $this->orders_count();

    $form['node'] = [
      '#type' => 'item',
      '#markup' => $orders_count,
    ];

    $form['submit_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start Orders Update'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity_ids = $this->get_order_ids();
    $data = array();
    $insert_count =  0;
    $batch = array(
      'title' => t('Update Records batch operation under progress'),
      'operations' => array(),
      'finished' => 'onFinishBatchCallbackFunc',
      'progress_message' => t('Update success @current out of @total records.'),
      'init_message' => t('Record update is starting.'),
    );


    $batch = mapped_data($entity_ids, $batch, $node = false);

    batch_set($batch);
  }

  /**
   * Get nodes from drupal 7 site
   * @return
   */
  public function get_order_ids() {
    $query = \Drupal::entityQuery('commerce_order');
    $entity_ids = $query->execute();

    return $entity_ids;
  }

/**
 * get all node records from drupal 7 and 8 for comparison
 * @return
 */
function orders_count() {
  $contentData = $this->get_order_ids();

  $options = ['absolute' => TRUE];

  $total_nodes_count = count($contentData);

  $table = "<table><tr><th>SN.</th><th>Node title on the current site</th><th>Node title on the drupal 7 site</th><th>Is Title Same?</th></tr>";
  $i = 1;

  // check if the development is in localhost
  $d7_connection = Database::getConnection('default', 'db_with_prefix');

  $same_title_count = 0;
  $different_title_count = 0;
  foreach ($contentData as $order_id) {
    $node_data = Order::load($order_id);
    $customer_id = $node_data->getCustomerId();
    $customer = $node_data->getCustomer();
    $email = $node_data->getEmail();
    $billing_profile = $node_data->getBillingProfile();
    $order_number = $node_data->getOrderNumber();

    $d7_query = $d7_connection->select('commerce_order', 'nd');
    $d7_query->addField('nd', 'order_id');
    $d7_query->fields('nd', []);
    $d7_query->condition('order_id', $order_id);

    $result = $d7_query->execute();
    $d7content = $result->fetch();

    $d7_mail = $d7content->mail??'';
    $d7_uid = $d7content->uid??'';
    $d7_order_id = $d7content->order_id??'';
    $d7_order_number = $d7content->order_number??'';


    if(($email == $d7_mail) && ($order_id == $d7_order_id)) {
      $same_title_count++;
      $equals = "<span class='success'>Same</span>";
    } else {
      $different_title_count++;
      $equals = "<span class='primary'>Not same</span>";
    }

    $table .= "<tr><td>{$i}</td><td>{$email} - {$order_id}</td><td>{$d7_mail} - {$d7_order_id}</td><td>{$equals}</td></tr>";
    $i++;
  }

  $table .= "</table>";
  $heading =  '<h5>Total nodes on the system: '.$total_nodes_count.'</h5>';
  $footer =  '<h3>Total identical orders: '.$same_title_count.'</h3>';
  $footer .=  '<h3>Total different orders: '.$different_title_count.'</h3>';
  $row = $heading.$table.$footer;

  return $row;
}

}
